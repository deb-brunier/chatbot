from api_key import *
import os
import openai

openai.api_key = OPENAI_KEY
completion = openai.Completion()
chat_log = '''Human: Salut, comment ça va?
AI: Je vais bien, merci? Quoi de neuf aujourd'hui? 
'''

def ask(question, chat_log):
    response = completion.create(
    prompt=f'{chat_log}\nHuman: {question}\nAI:',
        engine='text-davinci-003',
        stop=['\nHuman'],
        temperature=1,
        top_p=0,
        frequency_penalty=1,
        presence_penalty=0.6,
        best_of=1,
        max_tokens=150
    )
    return response.choices[0].text.strip()

while True:
    question = input('Human:')
    chat_log = f'{chat_log}Human: {question}\n'
    answer = ask(f'Human:{question}', chat_log)
    print(answer)
    chat_log = f'{chat_log}AI: {answer}\n'
