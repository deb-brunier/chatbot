<h2 align="center">CHATBOT</h2>
<p align="center"><a href="#" style="pointer-events: none;"><img src="https://img.shields.io/badge/python-FDDA0D?&style=for-the-badge" alt="PYTHON" title="PYTHON" height="25"></a></p>

<p>
Blabla
</p>

<h3>INSTALLATION</h3>
<h4>This project is running on [python@3.11](https://peps.python.org/pep-0664/):</h4>
- MacOS: with Homebrew ``` brew install python@3.11 ```
- Linux: ``` sudo apt-get install python 3 ```
- Windows: [version 3.11](https://www.python.org/downloads/windows/)


<h4>Packages:</h4>

- [PIP](https://pip.pypa.io/en/stable/installation/):
  - MacOS: download script [get-pip.py](https://bootstrap.pypa.io/get-pip.py), ```cd``` to the folder contaiing the get-pip.py file and run and execute  ```python get-py.py```<br>
  - Linux: ```apt-get install python3-pip```<br>
  - Windows: download script [get-pip.py](https://bootstrap.pypa.io/get-pip.py), ```cd``` to the folder containing the get-pip.py file and run and execute ```py get-pip.py```<br>

<h4>Environment:</h4>
- Linux & Mac:
#Si besoin sudo apt install python3-venv python -m venv chatbot
source chatbot/bin/activate
(chatbot) $ pip install openai

- Windows:
python -m venv chatbot
chatbot/Scripts/activate
(chatbot) $ pip install openai

<h3>CONTRIBUTORS</h3>
If you would like to contribute to this repository, feel free to send a pull request, and I will review your code. Also feel free to post about any problems that may arise in the issues section of the repository.